<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 *
 * Creates additional install settings.
 */
function brainstorm_profile_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  if ($form_id == 'install_configure_form') {
    // Add new option at configure site form. If checkbox was selected, we
    // enable custom module, which sends usage statistics.
    $form['additional_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Additional settings'),
      '#collapsible' => FALSE,
    ];

    $form['additional_settings']['send_message'] = [
      '#type' => 'checkbox',
      '#title' => t('Send info to developers team'),
      '#description' => t('You can send us the anonymous data about your site (URL and site-name). If you have any problems it can help us fix them.'),
      '#default_value' => TRUE,
    ];

    $form['#submit'][] = 'brainstorm_profile_install_configure_form_custom_submit';
  }
}

/**
 * Submit callback.
 *
 * @see system_form_install_configure_form_alter()
 */
function brainstorm_profile_install_configure_form_custom_submit($form, FormStateInterface &$form_state) {
  if ($form_state->getValue('send_message')) {
    \Drupal::service('module_installer')->install(['profile_stat_sender']);
  }
}

/**
 * Implements hook_profile_install_tasks_alter().
 */
function brainstorm_profile_install_tasks_alter(&$tasks, $install_state) {
  foreach ($install_state as $state) {
    switch ($state) {
      case 'install_bootstrap_full':
        $source = __DIR__ . '/libraries/';
        $res = 'libraries/';
        brainstorm_profile_recurse_copy($source, $res);
        drupal_get_messages();
        break;
      case 'install_profile_themes':
        brainstorm_profile_create_blocks();
        drupal_get_messages();
        break;
    }
  }
}

/**
 * Recursive copy.
 *
 * @param string $src
 *   - Source folder with files.
 * @param string $dst
 *   - Destination folder.
 */
function brainstorm_profile_recurse_copy($src, $dst) {
  if (is_dir($src)) {
    $dir = opendir($src);
    @mkdir($dst);
    while (FALSE !== ($file = readdir($dir))) {
      if (($file != '.') && ($file != '..')) {
        if (is_dir($src . '/' . $file)) {
          brainstorm_profile_recurse_copy($src . '/' . $file, $dst . '/' . $file);
        } else {
          copy($src . '/' . $file, $dst . '/' . $file);
        }
      }
    }
    closedir($dir);
  }
}

/**
 * Adds a necessary blocks into database.
 */
function brainstorm_profile_create_blocks() {
  // Create into block for About us.
  $block_content = BlockContent::create([
    'info' => 'About us',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '9d65bc41-a8ec-4a23-9443-1a4bbce52397',
    'body' => [
      'value' => 'Product is the vital sense of what we do,and truly great
        product is born of meticulous planning ad process.',
    ],
  ]);
  $block_content->save();

  // Create into block for Back to portfolio.
  $block_content = BlockContent::create([
    'info' => 'Back to portfolio',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '8a1a4f97-bc36-44b9-9a88-3059f5aab8ab',
    'body' => [
      'value' => '<div class="back-to"><a href="/portfolio">
        <input type="button" value="Back to portfolio"></a></div>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Colors.
  $block_content = BlockContent::create([
    'info' => 'Colors',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '1df14b7f-28e9-4739-a7b7-4fb97762c035',
    'body' => [
      'value' => '<div class="colors">
        <p>FOR ACCENTS</p>
        <div class="color-1"></div>
        <p>FOR ACCENTS</p>
        <div class="color-2"></div>
        <p>FOR BODY TEXT</p>
        <div class="color-3"></div>
        <p>FOR TITLE</p>
        <div class="color-4"></div>
        </div>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Company name.
  $block_content = BlockContent::create([
    'info' => 'Company name',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '33a160a5-811d-463d-985d-3e043da948c7',
    'body' => [
      'value' => '<div class="company-name"><div class="first-part picture"></div>
        <div class="second-part">
        <h3>Company name</h3>
        <p>Our company is a complete web design, web development and offshore outsourcing company, which brings ideas and technology for the success of your business. We deliver quality and innovative web design and web development solutions for localand international customers.
        <a href="/portfolio" class="button">our portfolio</a>
        </p>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Contact us.
  $block_content = BlockContent::create([
    'info' => 'Contact us',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => 'c5bc1b7c-a7d5-4358-92bd-c552d0cb0a48',
    'body' => [
      'value' => 'If you got any questions please do not hesitate to send us a message',
    ],
  ]);
  $block_content->save();

  // Create into block for Get in touch with us.
  $block_content = BlockContent::create([
    'info' => 'Get in touch with us',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '5787f51d-1947-4441-9e8c-93ce8506b652',
    'body' => [
      'value' => '<div class=\'contact-us\'><h3>GET IN TOUCH WITH US!</h3>
        <div class=\'phone icon\'>
        <h4>Give Us a Call </h4>
        <p>(0086) 9647 578924 <br>
        (048) 587 6459</p>
        </div>
        <div class=\'address icon\'>
        <h4>Where We Are?</h4>
        <p>86, Orchard, New York, NY 100002<br>
        United States</p>
        </div>
        <div class=\'mail icon\'>
        <h4>Drop us a Line</h4>
        <p>support@ctheme.com<br>
        hello@ctheme.com</p>
        </div>
        </div>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Lists.
  $block_content = BlockContent::create([
    'info' => 'Lists',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => 'd75c46db-446f-4f03-bd80-12841c873ced',
    'body' => [
      'value' => '<ul>
        <li><span>Home</span></li>
        <li><span>About us</span></li>
        <li><span>Portfolio</span></li>
        <li><span>Blog</span></li>
        <li><span>Typography</span></li>
        </ul>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Our skills.
  $block_content = BlockContent::create([
    'info' => 'Our skills',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '2df2096b-a296-4588-89ea-bead7a05f4ab',
    'body' => [
      'value' => '<div class="our-skills">
        <div class="first-part"><div class="team"><p>TEAM UP</p></div></div>
        <div class="second-part">
        <h2>Our skills</h2>
        <ul class="skills-bar-container">
          <p><label>Market Analysis</label>
          <div class="percent" id="pourcent-1">95%</div></p>
          <li>
            <div class="progressbar" id="progress-1"></div>
          </li>
          <p><label>Brand Identity</label>
          <div class="percent" id="pourcent-2">85%</div></p>
          <li>
            <div class="progressbar" id="progress-2"></div>
          </li>
           <p> <label>Graphic Design</label>
          <div class="percent" id="pourcent-3">99%</div></p>
          <li>
            <div class="progressbar" id="progress-3"></div>
          </li>
            <p><label>HTML Coding</label>
          <div class="percent" id="pourcent-4">80%</div></p>
            <li>
              <div class="progressbar" id="progress-4"></div>
            </li>
          </ul>
        </div>
        </div>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Social icons.
  $block_content = BlockContent::create([
    'info' => 'Social icons',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '902ee977-db6c-4f9f-be36-085c15184461',
    'body' => [
      'value' => '<a class="fa  fa-facebook-official" href="#"></a>
        <a class="fa fa-twitter" href="#"></a>
        <a class="fa fa-instagram " href="#"></a>
        <a class="fa fa-behance" href="#"></a>
        <a class="fa fa-linkedin-square" href="#"></a>
        <a class="fa fa-dribbble" href="#"></a>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();

  // Create into block for Who we are.
  $block_content = BlockContent::create([
    'info' => 'Who we are',
    'type' => 'basic',
    'langcode' => 'en',
    'uuid' => '3ca2a2cb-5811-4b72-ac36-c6c76e560710',
    'body' => [
      'value' => '<div class="about-us">
        <div class="first-part">
        <h2> Who we are</h2>
           <div class="text-who"><p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore eriatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatm, quia voluptas sit.</p></div>
        <a href="/about-us">More about us</a></div>
        <div class="second-part"></div>
        </div>',
      'format' => 'full_html',
    ],
  ]);
  $block_content->save();
}

/**
 * Brainstorm_profile clean alias.
 *
 * @param string $text
 *   String that be changing.
 *
 * @return string
 *   Return machine name for text.
 */
function _brainstorm_profile_clean_alias($text) {
  return preg_replace('/\-+/', '-', strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '', str_replace(' ', '-', $text))));
}
