<?php

/**
 * @file
 * Install, update and uninstall functions for the spark install profile.
 */

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\user\RoleInterface;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function brainstorm_profile_install() {
  // First, do everything in standard profile.
  include_once DRUPAL_ROOT . '/core/profiles/standard/standard.install';
  standard_install();

  // Set front page to "node".
  \Drupal::configFactory()
    ->getEditable('system.site')
    ->set('page.front', '/home')
    ->save(TRUE);

  // Set default user picture.
  $filename = 'social_270_270.png';
  $file_uuid = '5a157edd-b16c-402b-89a4-46d99e255964';
  $output = file_get_contents(drupal_get_path('profile', 'brainstorm_profile') . '/images/' . $filename);
  file_save_data($output, 'public://brainstorm-placeholder/' . $filename, FILE_EXISTS_REPLACE);
  $file_id = Drupal::entityQuery('file')
    ->condition('filename', $filename)
    ->execute();
  File::load(reset($file_id))->set('uuid', $file_uuid)->save();

  // Allow visitor account creation with administrative approval.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)
    ->save(TRUE);

  // Enable default permissions for system roles.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['search content']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['search content']);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->addRole('administrator');
  $user->save();

  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access site-wide contact form']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access site-wide contact form']);

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Enable the admin theme.
  \Drupal::configFactory()
    ->getEditable('node.settings')
    ->set('use_admin_theme', TRUE)
    ->save(TRUE);

  // Create link to Home page.
  MenuLinkContent::create([
    'title' => 'Home',
    'link' => ['uri' => 'internal:/home'],
    'menu_name' => 'footer',
    'weight' => '-4',
  ])->save();

  // Create link to About us page.
  MenuLinkContent::create([
    'title' => 'About us',
    'link' => ['uri' => 'internal:/about-us'],
    'menu_name' => 'footer',
    'weight' => '-3',
  ])->save();

  // Create link to Portfolio page.
  MenuLinkContent::create([
    'title' => 'Portfolio',
    'link' => ['uri' => 'internal:/portfolio'],
    'menu_name' => 'footer',
    'weight' => '-2',
  ])->save();

  // Create link to Blog page.
  MenuLinkContent::create([
    'title' => 'Blog',
    'link' => ['uri' => 'internal:/blog'],
    'menu_name' => 'footer',
    'weight' => '-1',
  ])->save();

  // Create link to Typography page.
  MenuLinkContent::create([
    'title' => 'Typography',
    'link' => ['uri' => 'internal:/typography'],
    'menu_name' => 'footer',
    'weight' => '1',
  ])->save();

  // Create link to Home page.
  MenuLinkContent::create([
    'title' => 'Home',
    'link' => ['uri' => 'internal:/home'],
    'menu_name' => 'main',
    'weight' => '-50',
  ])->save();

  // Create link to About us page.
  MenuLinkContent::create([
    'title' => 'About us',
    'link' => ['uri' => 'internal:/about-us'],
    'menu_name' => 'main',
    'weight' => '-49',
  ])->save();

  // Create link to Portfolio page.
  MenuLinkContent::create([
    'title' => 'Portfolio',
    'link' => ['uri' => 'internal:/portfolio'],
    'menu_name' => 'main',
    'weight' => '-48',
  ])->save();

  // Create link to Blog page.
  MenuLinkContent::create([
    'title' => 'Blog',
    'link' => ['uri' => 'internal:/blog'],
    'menu_name' => 'main',
    'weight' => '-47',
  ])->save();

  // Create link to Contact page.
  MenuLinkContent::create([
    'title' => 'Contact',
    'link' => ['uri' => 'internal:/contact'],
    'menu_name' => 'main',
    'weight' => '-46',
  ])->save();

  // Create link to Typography page.
  MenuLinkContent::create([
    'title' => 'Typography',
    'link' => ['uri' => 'internal:/typography'],
    'menu_name' => 'main',
    'weight' => '-45',
  ])->save();
}
