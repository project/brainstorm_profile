api = 2
core = 8.x

; Modules

projects[addtoany][version] = "1.15"

projects[blazy][version] = "2.4"

projects[config_update][version] = "1.7"

projects[ctools][version] = "3.7"

projects[easy_breadcrumb][version] = "1.15"

projects[features][version] = "3.12"

projects[flippy][version] = "1.0-beta4"

projects[libraries][version] = "3.x-dev"

projects[pathauto][version] = "1.8"

projects[slick][version] = "2.3"

projects[slick_views][version] = "2.4"

projects[token][version] = "1.9"

projects[colorized_gmap][version] = "1.2"

; Libraries
libraries[blazy][directory_name] = "blazy"
libraries[blazy][type] = "library"
libraries[blazy][destination] = "libraries"
libraries[blazy][download][type] = "git"
libraries[blazy][download][url] = "https://github.com/dinbror/blazy.git"
libraries[blazy][download][tag] = "1.8.2"

libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][download][type] = "get"
libraries[colorpicker][download][url] = "http://www.eyecon.ro/colorpicker/colorpicker.zip"

libraries[slick][directory_name] = "slick"
libraries[slick][type] = "library"
libraries[slick][destination] = "libraries"
libraries[slick][download][type] = "git"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick.git"
libraries[slick][download][tag] = "v1.8.1"

libraries[easings][directory_name] = "easings"
libraries[easings][type] = "library"
libraries[easings][destination] = "libraries"
libraries[easings][download][type] = "git"
libraries[easings][download][url] = "https://github.com/gdsmith/jquery.easing.git"
libraries[easings][download][tag] = "1.4.1"
